package model

import "go.mongodb.org/mongo-driver/bson/primitive"

//creating struct
type Course struct {
	ID    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Title string             `json:"Title,omitempty" bson:"Title,omitempty"`
}
type Rating struct {
	ID       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Name     string             `json:"Name,omitempty" bson:"Name,omitempty"`
	CourseID primitive.ObjectID `json:"CourseID,omitempty" bson:"CourseID,omitempty"`
	Rating   float64            `json:"Rating,omitempty" bson:"Rating,omitempty"`
	Feedback string             `json:"Feedback,omitempty" bson:"Feedback,omitempty"`
}
type RatingAvg struct {
	Title         string  `json:"Title,omitempty" bson:"Title,omitempty"`
	AverageRating float64 `json:"Rating,omitempty" bson:"Rating,omitempty"`
	Name          string  `json:"Name,omitempty" bson:"Name,omitempty"`
	Feedback      string  `json:"Feedback,omitempty" bson:"Feedback,omitempty"`
}
