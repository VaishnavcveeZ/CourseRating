package app

import (
	"log"
	"net/http"

	"gitlab.com/VaishnavcveeZ/CourseRating/controller"

	"github.com/gorilla/mux"
)

type App struct {
	router *mux.Router
}

//routers
func (app *App) InitialiseRoutes() {
	app.router = mux.NewRouter()
	app.router.HandleFunc("/addCourse", controller.AddCourse).Methods("POST")
	app.router.HandleFunc("/addRating", controller.AddRating).Methods("POST")
	app.router.HandleFunc("/allRating", controller.AllRating).Methods("GET")
	app.router.HandleFunc("/avgRating", controller.AvgRating).Methods("GET")
	app.router.HandleFunc("/allCourse", controller.AllCourse).Methods("GET")
	app.router.HandleFunc("/ratingDetails/{id}", controller.RatingDetails).Methods("GET")

}

//connecting to port to start local serer
func (app *App) Run() {
	log.Fatal(http.ListenAndServe(":12323", app.router))
}
