package connection

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client *mongo.Client

//connect to mongodb
func Connection() (*mongo.Client, error) {
	var err error
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err = mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		return nil, err
	}
	return client, nil
}

//connecting to database
func Database(database string, collection string) *mongo.Collection {
	collect := client.Database(database).Collection(collection)
	return collect
}
