package main

import (
	"fmt"

	"gitlab.com/VaishnavcveeZ/CourseRating/app"
	"gitlab.com/VaishnavcveeZ/CourseRating/connection"
)

func main() {
	connection.Connection() //connecting to mongodb
	App := app.App{}
	App.InitialiseRoutes()
	fmt.Println("Server Started ! --port :12323")
	App.Run() //starting the server
}
