package controller

import (
	"context"
	"encoding/json"
	"math"
	"net/http"
	"time"

	"gitlab.com/VaishnavcveeZ/CourseRating/connection"
	"gitlab.com/VaishnavcveeZ/CourseRating/model"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Api end point controller to add a course
func AddCourse(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")

	var course model.Course
	json.NewDecoder(request.Body).Decode(&course) //decoding course details

	//checking the given data is correct or not
	if len(course.Title) <= 0 {
		response.WriteHeader(http.StatusNotAcceptable)
		response.Write([]byte(`{"Title is empty !"}`))
		return
	}

	//inserting data to course collection
	collection := connection.Database("CourseRating", "Course")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	result, err := collection.InsertOne(ctx, course) //inserting course to collection
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	json.NewEncoder(response).Encode(result) //encoding result to response as a json format
}

//Api end point controller to add a rating to course
func AddRating(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")

	var rating model.Rating
	json.NewDecoder(request.Body).Decode(&rating)

	//checking the given data is correct or not
	if len(rating.Name) <= 0 || len(rating.CourseID) <= 0 || rating.Rating <= 0 || rating.Rating > 5 {
		response.WriteHeader(http.StatusNotAcceptable)
		response.Write([]byte(`{" Data is incorrect !"}`))
		return
	}

	//checking if the courseID is existing in course collection
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collectionN := connection.Database("CourseRating", "Course")
	var course model.Course
	err := collectionN.FindOne(ctx, model.Course{ID: rating.CourseID}).Decode(&course)
	if err != nil {
		response.WriteHeader(http.StatusNotAcceptable)
		response.Write([]byte(`{" Data is incorrect !, error : Course not found !"}`))
		return
	}

	//inserting data to rating collection
	collection := connection.Database("CourseRating", "Rating")
	result, err := collection.InsertOne(ctx, rating)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(response).Encode(result)
}

//Api end point controller to view all ratings of all courses
func AllRating(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")

	var ratings []model.Rating
	collection := connection.Database("CourseRating", "Rating")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.M{}) //fetching all ratings
	if err != nil || !(cursor.Next(ctx)) {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": data not found "` + err.Error() + `"}`))
		return
	}

	//itrating through fetched ratings
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var rating model.Rating
		cursor.Decode(&rating)
		ratings = append(ratings, rating)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(response).Encode(ratings)
}

//Api end point controller to view all courses
func AllCourse(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var courseList []model.Course
	collection := connection.Database("CourseRating", "Course")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})

	if err != nil || !(cursor.Next(ctx)) {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": data Not found "` + err.Error() + `"}`))
		return
	}
	defer cursor.Close(ctx)

	//itrating through fetched courses
	for cursor.Next(ctx) {
		var course model.Course
		cursor.Decode(&course)
		courseList = append(courseList, course)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(response).Encode(courseList)
}

//Api end point controller to get average rating of all course
func AvgRating(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	collection := connection.Database("CourseRating", "Course")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	Avg := map[string]float64{}
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var course model.Course
		cursor.Decode(&course)
		courseId := course.ID
		res, err := Average(courseId)
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(`{"message": "` + err.Error() + `"}`))
			return
		}
		Avg[course.Title] = math.Round(res*10) / 10

	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}

	json.NewEncoder(response).Encode(Avg)
}

//funcion to find average rating of a course
func Average(course primitive.ObjectID) (float64, error) {
	collection := connection.Database("CourseRating", "Rating")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.M{"CourseID": course})

	if !(cursor.Next(ctx)) || err != nil {
		return 0, err
	}
	var count float64
	var sum float64
	for cursor.Next(ctx) {
		count++
		var rating model.Rating
		cursor.Decode(&rating)
		sum += rating.Rating
	}
	return sum / count, err
}

//Api end point controller to view all rating and average rating of a perticular courses
func RatingDetails(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")

	params := mux.Vars(request)
	id, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	collection := connection.Database("CourseRating", "Rating")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cursor, err := collection.Find(ctx, bson.M{"CourseID": id})

	//checking if the course have rating
	if !(cursor.Next(ctx)) {
		var ratingList []model.RatingAvg
		var avgR model.RatingAvg
		var course model.Course
		collectionN := connection.Database("CourseRating", "Course")
		collectionN.FindOne(ctx, model.Course{ID: id}).Decode(&course)
		avgR.Title = course.Title
		ratingList = append(ratingList, avgR)
		json.NewEncoder(response).Encode(ratingList)
		return
	}
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	var ratingList []model.RatingAvg
	var avgR model.RatingAvg
	ratingList = append(ratingList, avgR)

	var count float64
	var sum float64

	for cursor.Next(ctx) {
		count++
		var rat model.RatingAvg
		var rating model.Rating
		cursor.Decode(&rating)
		rat.Name = rating.Name
		rat.Feedback = rating.Feedback
		rat.AverageRating = rating.Rating
		ratingList = append(ratingList, rat)
		sum += rating.Rating
	}

	avg := 0.0
	avg = math.Round((sum/count)*10) / 10
	collectionN := connection.Database("CourseRating", "Course")
	var course model.Course
	collectionN.FindOne(ctx, model.Course{ID: id}).Decode(&course)
	avgR.Title = course.Title
	avgR.AverageRating = avg
	ratingList[0] = avgR
	json.NewEncoder(response).Encode(ratingList)
}
